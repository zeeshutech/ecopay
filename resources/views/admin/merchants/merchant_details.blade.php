@extends('admin.layout.base')
@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN STYLE CUSTOMIZER -->
			<div class="theme-panel hidden-xs hidden-sm">
				<div class="toggler">
				</div>
				<div class="toggler-close">
				</div>
				<div class="theme-options">
					<div class="theme-option theme-colors clearfix">
						<span>
						THEME COLOR </span>
						<ul>
							<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">
							</li>
							<li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">
							</li>
							<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
							</li>
							<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
							</li>
							<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
							</li>
							<li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">
							</li>
						</ul>
					</div>
					<div class="theme-option">
						<span>
						Layout </span>
						<select class="layout-option form-control input-small">
							<option value="fluid" selected="selected">Fluid</option>
							<option value="boxed">Boxed</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Header </span>
						<select class="page-header-option form-control input-small">
							<option value="fixed" selected="selected">Fixed</option>
							<option value="default">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Mode</span>
						<select class="sidebar-option form-control input-small">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Menu </span>
						<select class="sidebar-menu-option form-control input-small">
							<option value="accordion" selected="selected">Accordion</option>
							<option value="hover">Hover</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Style </span>
						<select class="sidebar-style-option form-control input-small">
							<option value="default" selected="selected">Default</option>
							<option value="light">Light</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Position </span>
						<select class="sidebar-pos-option form-control input-small">
							<option value="left" selected="selected">Left</option>
							<option value="right">Right</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Footer </span>
						<select class="page-footer-option form-control input-small">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
				</div>
			</div>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Merchant Management <small>Edit Merchant</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="{{route('admin.dashboard')}}">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Merchant Management</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Edit Merchant</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->

			<div class="row">
				<div class="col-md-12">
				<div class="portlet light bg-inverse">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject font-red-sunglo bold uppercase">Merchant Profile</span>
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="" class="form-horizontal" method="post">
							@csrf
                                {{method_field('PUT')}}
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Merchant Legal & Commercial Name</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->business_name}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Type of Business </label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->type_of_business}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Name of Owner/CEO</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->name}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Name of Directors</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->	director_name}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Associated with a Group</label>
										<div class="col-md-4">
                                        <div class="radio-list">
										
												<label class="radio-inline">
												
												@if($merchant->associated_group)
												<span class=""><input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked="true"></span> Yes </label>
												
												@else
												<label class="radio-inline">
												<span class="checked"><input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" checked="true"></span> No </label>
												@endif
                                        </div>										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Group Name</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->	group_name}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Group Introduction</label>
										<div class="col-md-4">
											<textarea  name="name" class="form-control" value="" readonly>{{$merchant->group_introduction}}</textarea>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Annual Report</label>
										<div class="col-md-4">
                                            <textarea  name="name" class="form-control" value="" readonly>{{$merchant->annual_report}}</textarea>
                                        </div>
									</div>
									<!-- <div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">
												<i class="fa fa-envelope"></i>
												</span>
												<input type="email" name="email" class="form-control" placeholder="Email Address" value="">
											</div>
										</div>
									</div> -->
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>

            <div class="row">
				<div class="col-md-12">
				<div class="portlet light bg-inverse">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject font-red-sunglo bold uppercase">Business Details</span>
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="" class="form-horizontal" method="post">
							@csrf
                                {{method_field('PUT')}}
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Nature of business</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->nature_of_business}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Type of Business </label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->type_of_business}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Types of product and services sold</label>
										<div class="col-md-4">
											<textarea  name="name" class="form-control"  value="" readonly>{{$merchant->types_of_services_products}}</textarea>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Name of Directors</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->director_name}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Business Inception Date</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->business_inception_date}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Website URL</label>
										<div class="col-md-4">
											<input type="text" name="name" class="form-control" placeholder="" value="{{$merchant->website}}" readonly>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">
												<i class="fa fa-envelope"></i>
												</span>
												<input type="email" name="email" class="form-control" placeholder="Email Address" value="{{$merchant->email}}">
											</div>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Goods delivered internationally</label>
										<div class="col-md-4">
                                        <div class="radio-list">
										@if($merchant->associated_group)
												<label class="radio-inline">
												<span class=""><input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked=""></span> Yes </label>
												@else
												<label class="radio-inline">
												<span class="checked"><input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" checked=""></span> No </label>
												@endif
												
                                        </div>
									    </div>
									    <!-- <div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">
												<i class="fa fa-envelope"></i>
												</span>
												<input type="email" name="email" class="form-control" placeholder="Email Address" value="">
											</div>
										</div>
									    </div> -->
								</div>

                                <div class="form-group">
										<label class="col-md-3 control-label">Country list</label>
										<div class="col-md-4">
											<textarea  name="name" class="form-control"  readonly></textarea>
										</div>
									</div>
								<div class="form-group">
                                    <div class="table-scrollable" style="width:85% !important;">
										<table class="table table-hover table-bordered">
											<thead>
											<tr>
												<th width="30%">&nbsp;</th>
												<th>Minimum</th>
												<th>Maximum</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td>Estimated Transaction Count</td>
												<td>{{$merchant->estimated_transaction_count_min}}</td>
												<td>{{$merchant->estimated_transaction_count_max}}</td>
											</tr>
											<tr>
												<td>Estimated Transaction Amount</td>
												<td>{{$merchant->estimated_transaction_amount_min}}</td>
												<td>{{$merchant->estimated_transaction_amount_max}}</td>
											</tr>
											</tbody>
										</table>
									</div>
									</div>

									<div class="form-group">
                                    <div class="table-scrollable"  style="width:85% !important;">
										<table class="table table-hover table-bordered">
											<thead>
											<tr>
												<th width="30%">&nbsp;</th>
												<th>Daily</th>
												<th>Monthly</th>
												<th>Yearly</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td>Average Transaction Count</td>
												<td>{{$merchant->average_transaction_count_daily}}</td>
												<td>{{$merchant->average_transaction_count_monthly}}</td>
												<td>{{$merchant->average_transaction_count_yearly}}</td>
											</tr>
											<tr>
												<td>Average Transaction Amount</td>
												<td>{{$merchant->average_transaction_amount_daily}}</td>
												<td>{{$merchant->average_transaction_amount_monthly}}</td>
												<td>{{$merchant->average_transaction_amount_yearly}}</td>
											</tr>
											</tbody>
										</table>
									</div>
									</div>

								</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bg-inverse">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject font-red-sunglo bold uppercase">DOCUMENTATION CHECKLIST</span>
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								</a>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="" class="form-horizontal" method="post">
							@csrf
                                {{method_field('PUT')}}
								<div class="form-actions top">
									<div class="btn-set pull-left">
										<label>Partnership Concern</label>
									</div>
								</div>
								<div class="form-body">
									<div class="form-group">
											<label class="col-md-3 control-label">CNIC</label>
											<div class="col-md-4">
												<a href="#" class="btn btn-circle blue">
																		<i class="fa fa-file-o"></i> Download </a>
											</div>
									</div>
									<div class="form-group">
											<label class="col-md-3 control-label">Account maintenance certificate*
</label>
											<div class="col-md-4">
												<a href="#" class="btn btn-circle blue">
																		<i class="fa fa-file-o"></i> Download </a>
											</div>
									</div>
									<div class="form-group">
											<label class="col-md-3 control-label">NTN</label>
											<div class="col-md-4">
												<a href="#" class="btn btn-circle blue">
																		<i class="fa fa-file-o"></i> Download </a>
											</div>
									</div>
									<div class="form-group">
											<label class="col-md-3 control-label">Partnership Deed</label>
											<div class="col-md-4">
												<a href="#" class="btn btn-circle blue">
																		<i class="fa fa-file-o"></i> Download </a>
											</div>
									</div>
								</div>
                            </form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection