<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<br>
				<li class="start {{ (request()->is('admin')) ? 'active' : '' }}">
					<a href="{{route('admin.dashboard')}}">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
                </li>
                <li class="{{ (request()->is('admin/users/create') || request()->is('admin/users') || request()->is('admin/users/*/edit')) ? 'active open' : '' }}">
					<a href="javascript:;">
					<i class="icon-users"></i>
					<span class="title">User Management</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li class="{{ (request()->is('admin/users/create')) ? 'active' : '' }}">
							<a href="{{route('users.create')}}">
							<i class="icon-user-follow"></i>
							Add User</a>
						</li>
						<li class="{{ (request()->is('admin/users')) ? 'active' : ''}}">
							<a href="{{route('users.index')}}">
							<i class="icon-user"></i>
							Manage User</a>
						</li>
					</ul>
                </li>
                <li class="{{ (request()->is('/admin/merchants')) ? 'active open' : '' }}">
					<a href="javascript:;">
					<i class="icon-users"></i>
					<span class="title">Merchants</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li class="{{ (request()->is('/admin/merchants')) ? 'active' : ''}}">
							<a href="{{route('admin.merchants.allmerchants')}}">
							<i class="icon-users"></i>
							Merchants</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-user-follow"></i>
							Add Merchant</a>
						</li>
					</ul>
                </li>
                <li>
					<a href="{{route('admin.merchants.all_transactions')}}">
					<i class="fa fa-money"></i>
					<span class="title">Transactions</span>
					</a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="fa fa-book"></i>
					<span class="title">Revenues</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#">
							<i class="fa fa-book"></i>
							Revenue1</a>
						</li>
						<li>
							<a href="#">
							<i class="fa fa-book"></i>
							Revenue2</a>
						</li>
					</ul>
                </li>
                <li>
					<a href="javascript:;">
					<i class="icon-envelope-open"></i>
                    <span class="title">Email Templates</span>
                    </a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="glyphicon glyphicon-phone"></i>
                    <span class="title">SMS Templates</span>
                    </a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="fa fa-ticket"></i>
					<span class="title">Promo Codes</span>
					</a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="fa fa-exchange"></i>
                    <span class="title">Transaction Rates</span>
                    </a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="icon-eye"></i>
                    <span class="title">Activity Logs</span>
                    </a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="fa fa-file"></i>
                    <span class="title">Reports</span>
                    </a>
                </li>
                <li>
					<a href="javascript:;">
					<i class="fa fa-cogs"></i>
                    <span class="title">System Config</span>
                    </a>
                </li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>