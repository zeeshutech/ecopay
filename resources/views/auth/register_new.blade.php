@extends('layouts.base_login')
@section('content')
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Partner Registeration</h1>
              </div>
              <form class="user" method="POST" action="{{ route('registeration') }}">
                  @csrf
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <select  class="form-control  " id="account_type" name
                    ="account_type">
                        <option>Account Type</option>
                        <option value="1">Merchant</option>
                        <option value="2">Aggregator</option>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="business_name" name="business_name" placeholder="Business Name">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control  " id="name" name="name" placeholder="Name">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control  " id="email" name="email" placeholder="Email Address">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control  " id="number" name="number" placeholder="Mobile No.">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control  " id="password" name="password" placeholder="Password">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control  " id="repeat_password" name="repeat_password" placeholder="Repeat Password">
                  </div>
                </div>
                  <button type="submit" class="btn btn-primary">
                  Submit
                  </button>
                <!-- <hr>
                <a href="index.html" class="btn btn-google btn-user btn-block">
                  <i class="fab fa-google fa-fw"></i> Register with Google
                </a>
                <a href="index.html" class="btn btn-facebook btn-user btn-block">
                  <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                </a> -->
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="login.html">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection
