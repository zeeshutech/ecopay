@extends('layouts.merchant_app')
@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Account Settings - Profile</h1>
</div>

<!-- Account page navigation-->
<nav class="nav nav-borders">
    <a class="nav-link active ml-0" href="account-profile.html">Profile</a>
    <a class="nav-link" href="account-billing.html">Password</a>
</nav>
<hr class="mt-0 mb-4">
<div class="row">
    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card">
            <div class="card-header">Profile Picture</div>
            <div class="card-body text-center">
                <!-- Profile picture image-->
                <img class="img-account-profile rounded-circle mb-2" src="https://source.unsplash.com/QAB-WJcbgJk/300x300" alt="">
                <!-- Profile picture help block-->
                <div class="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div>
                <!-- Profile picture upload button-->
                <button class="btn btn-primary" type="button">Upload new image</button>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <!-- Account details card-->
        <div class="card mb-4">
            <div class="card-header">Account Details</div>
            <div class="card-body">
                <form>
                    <!-- Form Group (username)-->
                    <div class="form-group">
                        <label class="small mb-1" for="inputUsername">Name</label>
                        <input class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="">
                    </div>

                    <!-- Form Row-->
                    <div class="form-row">
                       <!-- Form Group (email address)-->
                       <div class="form-group col-md-6">
                            <label class="small mb-1" for="inputEmailAddress">Email address</label>
                            <input class="form-control" id="inputEmailAddress" type="email" placeholder="Enter your email address" value="">
                        </div>

                        <!-- Form Group (phone number)-->
                        <div class="form-group col-md-6">
                            <label class="small mb-1" for="inputPhone">Phone number</label>
                            <input class="form-control" id="inputPhone" type="tel" placeholder="Enter your phone number" value="">
                        </div>
                    </div>
                    <!-- Form Row        -->
                    <div class="form-row">
                        <!-- Form Group (organization name)-->
                        <div class="form-group col-md-6">
                            <label class="small mb-1" for="inputOrgName">Business Name</label>
                            <input class="form-control" id="inputOrgName" type="text" placeholder="Enter your business name" value="">
                        </div>
                        <!-- Form Group (location)-->
                        <div class="form-group col-md-6">
                            <label class="small mb-1" for="inputLocation">Business Role</label>
                            <input class="form-control" id="inputLocation" type="text" value="">
                        </div>
                    </div>

                    <!-- Save changes button-->
                    <button class="btn btn-primary" type="button">Save changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
