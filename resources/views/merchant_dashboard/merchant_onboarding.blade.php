@extends('layouts.merchant_app')
{{--@extends('layouts.app')--}}
@section('content')
<style>
* {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 20px;
  width: 80%;
  min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width:100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}



/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
</style>

      <!-- Page Heading -->
      <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
      </div> -->

      

<form id="regForm" action="{{ route('documentation') }}" method="POST" enctype="multipart/form-data">
@csrf
{{method_field('POST')}}
  <h1>Register:</h1>
  <!-- One "tab" for each step in the form: -->
   <div class="tab">Merchant Profile:
   <p class="form-group"><input placeholder="Merchant Legal & Commercial Name..." oninput="this.className " name="business_name" id="business_name" value="{{$merchant->business_name}}"></p>
    <p class="form-group"><input placeholder="Name of Owner/CEO..." oninput="this.className " name="name" id="name" value="{{$merchant->name}}"></p>
    <p class="form-group">
        <label>Type of Business</label><br>
        <label class="form-group">
        <input type="radio" name="type_of_business" id="type_of_business" value="Proprietorship" oninput="this.className " checked>Proprietorship
        </label>
        <label class="form-group">
        <input type="radio" name="type_of_business" id="type_of_business" value="Partnership" oninput="this.className ">Partnership
        </label>
        <label class="form-group">
        <input type="radio" name="type_of_business" id="type_of_business" value="Limited_Co" oninput="this.className ">Limited Co 
        </label>
        <label class="form-group">
        <input type="radio" name="type_of_business" id="type_of_business" value="NGO/Club" oninput="this.className ">NGO/Club
        </label>
    </p>
    <p class="form-group"><input placeholder="Name of Directors (if applicable)..." oninput="this.className = ''" name="director_name" id="director_name" value="{{$merchant->director_name}}"></p>
    <!-- <p><label><input type="checkbox" oninput="this.className" value="">Associated with a Group</label></p> -->
    <p>
        <label for="form-group" >Associated with a Group 
        <input type="checkbox" id="associated_group"  name="associated_group" style="width:20px; height: 10px;"></label> 
    </p>
    <p><input placeholder="Group Name (if applicable)..." oninput="this.className = ''" name="group_name" id="group_name" value="{{$merchant->	group_name}}"></p>
    <p><div class="form-group">
            <label for="comment">Group Introduction (if applicable):</label>
            <textarea class="form-control" rows="3" oninput="this.className" id="group_introduction" name="group_introduction" value="">{{$merchant->group_introduction}}</textarea>
        </div>
    </p>
    <p><div class="form-group">
            <label for="comment">Annual Report (where applicable):</label>
            <textarea class="form-control" rows="3"  oninput="this.className" id="annual_report" name="annual_report" value="">{{$merchant->annual_report}}</textarea>
        </div>
    </p>
  </div>
  <div class="form-group tab">Business Details:
    <p><input placeholder="Nature of business..." oninput="this.className = ''" name="nature_of_business" id="nature_of_business" value="{{$merchant->nature_of_business}}"></p>
    <p><div class="form-group">
            <label for="comment">Types of product and services sold:</label>
            <textarea class="form-control" rows="3" oninput="this.className" id="types_of_services_products" name="types_of_services_products"  value="">{{$merchant->types_of_services_products}}</textarea>
        </div>
    </p>
    
    <p class="form-group"><span><Label>Business Inception Date</Label></span><span><input  type=date oninput="this.className = ''" name="business_inception_date" id="business_inception_date" value="{{$merchant->business_inception_date}}"></span></p>
    <p class="form-group"><input placeholder="Website URL..." oninput="this.className = ''" name="website"></p>
    <p class=""><input placeholder="Email Address..." type=email oninput="this.className = ''" name="email"></p>
    <p><div class="form-group">
            <label for="comment">Registered Business Address:</label>
            <textarea class="form-control" rows="5" oninput="this.className" id="comment" name="business_location"></textarea>
        </div>
    </p>
    <p class="form-group"><input placeholder="Delivery mode of product & services..." oninput="this.className = ''" name="lname"></p>
    <p>
        <div class="form-group">
        <span><Label>Goods delivered internationally</Label></span>
            <span class="radio">
                <label><input type="radio" name="goods_delivered_internationally" id="goods_delivered_internationally" value="1" oninput="this.className" style="width:20px">Yes</label>
            </span>
            <span class="radio">
                <label><input type="radio" name="goods_delivered_internationally" id="goods_delivered_internationally" value="1" oninput="this.className" checked style="width:20px">No</label>
            </span>
	    </div>
    </p>
    <p>
        <div class="form-group container">
            
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th>Minimum</th>
                    <th>Maximum</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Estimated Transaction Count</td>
                    <td><input  oninput="this.className " name="estimated_transaction_count_min" id="estimated_transaction_count_min"></p></td>
                    <td><input  oninput="this.className = ''" name="estimated_transaction_count_max" id="estimated_transaction_count_max"></p></td>
                </tr>
                <tr>
                    <td>Estimated Transaction Amount</td>
                    <td><input  oninput="this.className " name="estimated_transaction_amount_min"  id="estimated_transaction_amount_min"></p></td>
                    <td><input  oninput="this.className " name="estimated_transaction_amount_max" id="estimated_transaction_amount_max"></p></td>
                </tr>
                </tbody>
            </table>
        </div>
    </p>
    <p>
        <div class="form-group container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th></th>
                    <th>Daily</th>
                    <th>Monthly</th>
                    <th>Yearly</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Average Transaction Count</td>
                    <td><input  oninput="this.className " name="average_transaction_count_daily" id="average_transaction_count_daily"></p></td>
                    <td><input  oninput="this.className " name="average_transaction_count_monthly" id="average_transaction_count_monthly"></p></td>
                    <td><input  oninput="this.className " name="average_transaction_count_yearly" id="average_transaction_count_yearly"></p></td>
                </tr>
                <tr>
                    <td>Average Transaction Amount</td>
                    <td><input  oninput="this.className " name="average_transaction_amount_daily" id="average_transaction_amount_daily"></p></td>
                    <td><input  oninput="this.className " name="average_transaction_amount_monthly" id="average_transaction_amount_monthly"></p></td>
                    <td><input  oninput="this.className " name="average_transaction_amount_yearly" id="average_transaction_amount_yearly"></p></td>
                </tr>
                </tbody>
            </table>
        </div>
    </p>
  </div>
  <div class="tab">MERCHANT ONBOARDING DOCUMENTATION CHECKLIST:
    <p >
        <label for="myCheck" >Proprietorship Concern: 
        <input class="form-group" type="checkbox" name="myCheck" id="myCheck" onclick="myFunction()" style="width:20px; height: 10px;" ></label> 
        <label for="myCheck1" >Partnership Concern: 
        <input class="form-group" type="checkbox" name="myCheck1" id="myCheck1" onclick="myFunction1()" style="width:20px; height: 10px;"></label>
        <label  for="myCheck2" >Limited Company: 
        <input class="form-group" type="checkbox" name="myCheck2" id="myCheck2" onclick="myFunction2()" style="width:20px; height: 10px;"></label>
    </p>
    <div id="Proprietorship">
                        
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="account_maintanece_certificate" id="account_maintanece_certificate">
                <label class="custom-file-label" for="customFile">Account maintenance certificate</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="cnic" id="cnic">
                <label class="custom-file-label" for="customFile">CNIC Copy of owner / proprietor</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="ntn" id="ntn">
                <label class="custom-file-label" for="customFile">NTN</label>
            </div>
        </p>
        
    </div>
    <p >
        
    </p>
    <div id="Partnership">
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="p_cnic" id="p_cnic">
                <label class="custom-file-label" for="customFile">CNIC Copy</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="p_account_maintanece_certificate" id="p_account_maintanece_certificate">
                <label class="custom-file-label" for="customFile">Account maintenance certificate</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="partner_cnic" id="partner_cnic">
                <label class="custom-file-label" for="customFile">Partners CNIC</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="p_ntn" id="p_ntn">
                <label class="custom-file-label" for="customFile">NTN</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="partnership_deed" id="partnership_deed">
                <label class="custom-file-label" for="customFile">Partnership Deed</label>
            </div>
        </p>
    </div>
    <p>
         
    </p>
    <div id="Limited">
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="l_cnic" id="l_cnic">
                <label class="custom-file-label" for="customFile">CNIC Copy</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="l_account_maintanece_certificate" id="l_account_maintanece_certificate">
                <label class="custom-file-label" for="customFile">Account maintenance certificate</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="certificate_of_incorporation" id="certificate_of_incorporation">
                <label class="custom-file-label" for="customFile">Certificate of incorporation</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="fbr_registration_notification" id="fbr_registration_notification">
                <label class="custom-file-label" for="customFile">FBR registration notification</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="signed_letter_from_merchant_on_company_letter_head" id="signed_letter_from_merchant_on_company_letter_head">
                <label class="custom-file-label" for="customFile">Signed letter from the merchant on company letterhead mentioning merchant account to be credited</label>
            </div>
        </p>
        <p> 
            <div class="form-group custom-file">
                <input type="file" class="custom-file-input" name="board_resolution_to_verify_signatory_of_letter" id="board_resolution_to_verify_signatory_of_letter">
                <label class="custom-file-label" for="customFile">Board resolution to verify signatory of the letter</label>
            </div>
        </p>
    </div>
  </div>
  
  <!-- <div class="tab">Login Info:
    <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
    <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>
  </div> -->
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <!-- <span class="step"></span> -->
  </div>
</form>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  var text = document.getElementById("Proprietorship");
  var text1 = document.getElementById("Partnership");
  var text2 = document.getElementById("Limited");
  text.style.display = "none";
    text1.style.display = "none";
    text2.style.display = "none";
  
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    console.log(currentTab);
    
    
    //type="submit"
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // if (currentTab >2) {
  //   console.log("submit");
  // }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
 // console.log(x);
  // Exit the function if any field in the current tab is invalid:

  // alert(validateForm());return false; 
  //if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  //console.log('Before' + currentTab);
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  //console.log('After' + currentTab);
  if (currentTab >= x.length) {
    // ... the form gets submitted:
   
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}


// Add the following code if you want the name of the file appear on select
// $(".custom-file-input").on("change", function() {
//   var fileName = $(this).val().split("\\").pop();
//   $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
// });

function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var checkBox1 = document.getElementById("myCheck1");
  var checkBox2 = document.getElementById("myCheck2");
  var text = document.getElementById("Proprietorship");
  // var text1 = document.getElementById("Partnership");
  // var text2 = document.getElementById("Limited");
  // var cnic = document.getElementById("cnic");
  var account_maintanece_certificate = document.getElementById("account_maintanece_certificate");
  var ntn = document.getElementById("ntn");
  if (checkBox.checked == true){
    text.style.display = "block";
    // document.getElementById("cnic").required=true;
    checkBox1.checked=false;
    checkBox2.checked=false;
    text1.style.display = "none";
    text2.style.display = "none";

  }
  else{
    text.style.display = "none";
  }   
}
function myFunction1() {
  var checkBox = document.getElementById("myCheck");
  var checkBox1 = document.getElementById("myCheck1");
  var checkBox2 = document.getElementById("myCheck2");
  var text = document.getElementById("Proprietorship");
  var text1 = document.getElementById("Partnership");
  var text2 = document.getElementById("Limited");
  if (checkBox1.checked == true){
    text1.style.display = "block";
    checkBox.checked=false;
    checkBox2.checked=false;
    text.style.display = "none";
    text2.style.display = "none";
  }
  else{
    text1.style.display = "none";
  }  
}
function myFunction2() {
  var checkBox = document.getElementById("myCheck");
  var checkBox1 = document.getElementById("myCheck1");
  var checkBox2 = document.getElementById("myCheck2");
  var text = document.getElementById("Proprietorship");
  var text1 = document.getElementById("Partnership");
  var text2 = document.getElementById("Limited");
  if (checkBox2.checked == true){
    text2.style.display = "block";
    checkBox1.checked=false;
    checkBox.checked=false;
    text1.style.display = "none";
    text.style.display = "none";
  }
  else{
    text2.style.display = "none";
  } 
}

</script>
@endsection
