<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'pp_Version',
            'pp_TxnType',
            'pp_Language',
            'pp_MerchantID',
            'pp_SubMerchantID',
            'pp_TxnRefNo',
            'pp_Password',
            'pp_Amount',
            'pp_TxnCurrency',
            'pp_TxnDateTime',
            'pp_BillReference',
            'pp_Description',
            'pp_TxnExpiryDateTime',
            'pp_ReturnURL',
            'pp_ResponseCode',
            'pp_ResponseMessage',
            'pp_RetreivalReferenceNo',
            'pp_AuthCode',
            'pp_SettlementExpiry',
            'pp_BankID',
            'pp_ProductID',
            'pp_CNIC',
            'pp_AccountNo',
            'pp_SecureHash',
            'ppmpf_1',
            'ppmpf_2',
            'ppmpf_3',
            'ppmpf_4',
            'ppmpf_5',
    ];
}
