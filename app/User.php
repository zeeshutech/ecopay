<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','business_name','phone_number','account_type','type_of_business','director_name','associated_group','group_name','group_introduction','annual_report',
        'nature_of_business','types_of_services_products','business_inception_date','goods_delivered_internationally','estimated_transaction_count_min','estimated_transaction_count_max',
        'estimated_transaction_amount_min','estimated_transaction_amount_max','average_transaction_count_daily','average_transaction_count_monthly','average_transaction_count_yearly',
        'average_transaction_amount_daily','average_transaction_amount_monthly','average_transaction_amount_yearly','cnic','basic','account_maintanece_certificate','delivery_mode_of_product_services',
        'partnership','properitership','limited_company','ntn','partner_cnic','partnership_deed','certificate_of_incorporation','fbr_registration_notification',
        'signed_letter_from_merchant_on_company_letter_head','board_resolution_to_verify_signatory_of_letter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
