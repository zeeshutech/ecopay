<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('merchant_dashboard/dashboard');
    }
    public function merchant_onboarding(){
        $user_id=Auth::user()->id;
        $user=User::find($user_id);
        //dd($merchant_details);
        return view('merchant_dashboard/merchant_onboarding')->with(['merchant' =>$user]);
    }
    public function showMerchant($user){
       
    }
    public function merchant_onboarding_documentation(Request $request){
        //dd($_FILES);die;
        //dd($request->all());
        
        $user_id=Auth::user()->id;
        $user=User::find($user_id);

        //dd($user);
       
        //$a=$request->file('cnic');
        //dd($a);
        if($request['myCheck']){
            $cnic = $request->file('cnic')->storeAs(
                'cnic', 'cnic-'.$request->user()->id
            );
            $account_maintanece_certificate = $request->file('account_maintanece_certificate')->storeAs(
                'account_maintanece_certificate', 'account_maintanece_certificate-'.$request->user()->id
            );
            $ntn = $request->file('ntn')->storeAs(
                'ntn', 'ntn-'.$request->user()->id
            );
            $user->fill([
                'cnic'=> $cnic,
                'account_maintanece_certificate' => $account_maintanece_certificate,
                'ntn' => $ntn,
            ]);
        }
        else if($request['myCheck1']){
            $cnic = $request->file('p_cnic')->storeAs(
                'cnic', 'cnic-'.$request->user()->id
            );
            $account_maintanece_certificate = $request->file('p_account_maintanece_certificate')->storeAs(
                'account_maintanece_certificate', 'account_maintanece_certificate-'.$request->user()->id
            );
            $ntn = $request->file('ntn')->storeAs(
                'ntn', 'ntn-'.$request->user()->id
            );
            $partner_cnic = $request->file('p_ntn')->storeAs(
                'partner_cnic', 'partner_cnic-'.$request->user()->id
            );
            $partnership_deed = $request->file('partnership_deed')->storeAs(
                'partnership_deed', 'partnership_deed-'.$request->user()->id
            );
            $user->fill([
                'cnic'=> $cnic,
                'account_maintanece_certificate' => $account_maintanece_certificate,
                'ntn' => $ntn,
                'partner_cnic' => $partner_cnic,
                'partnership_deed' => $partnership_deed,
            ]);
        }
        else if($request['myCheck2']){
            $cnic = $request->file('l_cnic')->storeAs(
                'cnic', 'cnic-'.$request->user()->id
            );
            $account_maintanece_certificate = $request->file('l_account_maintanece_certificate')->storeAs(
                'account_maintanece_certificate', 'account_maintanece_certificate-'.$request->user()->id
            );
            $certificate_of_incorporation = $request->file('certificate_of_incorporation')->storeAs(
                'certificate_of_incorporation', 'certificate_of_incorporation-'.$request->user()->id
            );
            $fbr_registration_notification = $request->file('fbr_registration_notification')->storeAs(
                'fbr_registration_notification', 'fbr_registration_notification-'.$request->user()->id
            );
            $signed_letter_from_merchant_on_company_letter_head = $request->file('signed_letter_from_merchant_on_company_letter_head')->storeAs(
                'signed_letter_from_merchant_on_company_letter_head', 'signed_letter_from_merchant_on_company_letter_head-'.$request->user()->id
            );
            $board_resolution_to_verify_signatory_of_letter = $request->file('board_resolution_to_verify_signatory_of_letter')->storeAs(
                'board_resolution_to_verify_signatory_of_letter', 'board_resolution_to_verify_signatory_of_letter-'.$request->user()->id
            );
            $user->fill([
                'cnic'=> $cnic,
                'account_maintanece_certificate' => $account_maintanece_certificate,
                'certificate_of_incorporation' => $certificate_of_incorporation,
                'fbr_registration_notification' => $fbr_registration_notification,
                'signed_letter_from_merchant_on_company_letter_head' => $signed_letter_from_merchant_on_company_letter_head,
                'board_resolution_to_verify_signatory_of_letter' => $board_resolution_to_verify_signatory_of_letter,
            ]);
        }
        

        

        $user->fill([
            'name' => $request['name'],
            'business_name' => $request['business_name'],
            'type_of_business' => $request['type_of_business'],
            'director_name' => $request['director_name'],
            'associated_group' => $request['associated_group'],
            'group_name' => $request['group_name'],
            'group_introduction' => $request['group_introduction'],
            'annual_report' => $request['annual_report'],
            'nature_of_business' => $request['nature_of_business'],
            'types_of_services_products' => $request['types_of_services_products'],
            'business_inception_date' => $request['business_inception_date'],
            'website' => $request['website'],   
            'email' => $request['email'],   
            'business_location' => $request['business_location'],   
            'goods_delivered_internationally' => $request['goods_delivered_internationally'],
            'estimated_transaction_count_min' => $request['estimated_transaction_count_min'],
            'estimated_transaction_count_max' => $request['estimated_transaction_count_max'],
            'estimated_transaction_amount_min' => $request['estimated_transaction_amount_min'],
            'estimated_transaction_amount_max' => $request['estimated_transaction_amount_max'],
            'average_transaction_count_daily' => $request['average_transaction_count_daily'],
            'average_transaction_count_monthly' => $request['average_transaction_count_monthly'],
            'average_transaction_count_yearly' => $request['average_transaction_count_yearly'],
            'average_transaction_amount_daily' => $request['average_transaction_amount_daily'],
            'average_transaction_amount_monthly' => $request['average_transaction_amount_monthly'],
            'average_transaction_amount_yearly' => $request['average_transaction_amount_yearly'],
        ]);
        // dd($user);
        $user->save();

        // return User::update([
            
        //     'cnic' => $request['cnic'],
        //     'basic' => $request['basic'],
        //     'account_maintanece_certificate' => $request['account_maintanece_certificate'],
        //     'delivery_mode_of_product_services' => $request['delivery_mode_of_product_services'],
        //     'partnership' => $request['myCheck1'],
        //     'properitership' => $request['myCheck'],
        //     'limited_company' => $request['myCheck2'],
        //     'ntn' => $request['ntn'],
        //     'partner_cnic' => $request['partner_cnic'],
        //     'partnership_deed' => $request['partnership_deed'],
        //     'certificate_of_incorporation' => $request['certificate_of_incorporation'],
        //     'fbr_registration_notification' => $request['fbr_registration_notification'],
        //     'signed_letter_from_merchant_on_company_letter_head' => $request['signed_letter_from_merchant_on_company_letter_head'],
        //     'board_resolution_to_verify_signatory_of_letter' => $request['board_resolution_to_verify_signatory_of_letter'],


        // ]);
    }
}
