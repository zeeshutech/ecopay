<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins =  Admin::all();
        return view('admin.users.index')->with(['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create')->with(['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $admin = Admin::create([
            'name'  => $request->name,
            'email'  => $request->email,
            'password'  => $request->password,
        ]);
        
        if(!empty($admin) && $admin['id'] != ''){
            $admin->roles()->sync($request->role);
        }
        
        return redirect()->route('users.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $roles = Role::all();
        $admin = Admin::findorfail($id);
        $admin_role = $admin->roles->pluck('id')->first();
        return view('admin.users.edit')->with(['roles' => $roles,'admin_role' => $admin_role,'admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin,$id)
    {   
        $admin = Admin::findorfail($id);
        if($admin){
            Admin::where('id',$id)->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);

            $admin->roles()->sync($request->role);
        }

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($admin)
    {
        $admin = Admin::where('id',$admin)->first();
        $admin->roles()->detach();
        $admin->delete();
        return redirect()->route('users.index');
    }
}
