<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Admin;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function getMerchants(){
       $merchants = User::all();
       return view('admin.merchants.allmerchants')->with(['merchants' =>$merchants]);
    }

    public function showMerchant($user){
        $merchant_details=User::find($user);
        //dd($merchant_details);
        return view('admin.merchants.merchant_details')->with(['merchant' =>$merchant_details]) ;
    }
}

