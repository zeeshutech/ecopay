<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public/home');
})->name('home-new');
Route::get('/userLogout','Auth\LoginController@userLogout')->name('logout');
Route::post('/users/logout','Auth\LoginController@userLogout')->name('user.logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin routes
Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::POST('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

});

Route::resource('/admin/users', 'Admin\UsersController');

Route::post('/register', 'Auth\RegisterController@create')->name('registeration');

Route::get('/admin/merchants','Admin\AdminController@getMerchants')->name('admin.merchants.allmerchants');
Route::get('/admin/transactions','Admin\TransactionsController@getTransactions')->name('admin.merchants.all_transactions');
Route::get('/admin/merchant_detail/{user}','Admin\AdminController@showMerchant')->name('admin.merchants.merchant_details');


//Public site routes
Route::get('/new-register', function () {
    return view('auth/register_new');
})->name('new-register');

Route::get('/new-login', function () {
    return view('auth/login_new');
})->name('new-login');

Route::get('/merchant/dashboard', function () {
    return view('merchant_dashboard/dashboard');
});
Route::get('/merchant/merchant_onboarding', function () {
    return view('merchant_dashboard/merchant_onboarding');
})->name('merchant_onboarding');
Route::get('/merchant_onboarding', 'HomeController@merchant_onboarding')->name('onboarding');
Route::post('/merchant_documentation', 'HomeController@merchant_onboarding_documentation')->name('documentation');
Route::get('/merchant/profile', function () {
    return view('merchant_dashboard/profile');
})->name('user-profile');;



// Route::get('/transactions/getTransactionNotification', function(){

//     $results = \App\Transaction::all();
//     return $results;
// });