<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        DB::table('admin_role')->truncate();

        $adminRole = Role::where('name','admin')->first();
        $authorRole = Role::where('name','author')->first();
        $userRole = Role::where('name','user')->first();

        $admin = Admin::create([
            'name'  => 'admin',
            'email' => 'admin@ecopay.com',  
            'password'  => Hash::make('admin@123')
        ]);

        $author =  Admin::create([
            'name'  => 'author',
            'email' => 'author@ecopay.com',  
            'password'  => Hash::make('auhtor@123')
        ]);

        $user =  Admin::create([
            'name'  => 'user',
            'email' => 'user@ecopay.com',  
            'password'  => Hash::make('user@123')
        ]);

        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
        $user->roles()->attach($userRole);

    }
}
