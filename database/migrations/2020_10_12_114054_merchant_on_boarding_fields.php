<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MerchantOnBoardingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('type_of_business',255)->after('physical_address')->nullable();
            $table->string('director_name',255)->nullable();
            $table->boolean('associated_group')->nullable();
            $table->string('group_name',255)->nullable();
            $table->text('group_introduction')->nullable();
            $table->text('annual_report',255)->nullable();
            $table->string('nature_of_business',255)->nullable();
            $table->string('types_of_services_products',255)->nullable();
            $table->timestamp('business_inception_date')->nullable();
            //$table->string('delivery_mode_of_product_services',255)->nullable();
            $table->boolean('goods_delivered_internationally')->nullable();
            $table->integer('estimated_transaction_count_min')->nullable();
            $table->integer('estimated_transaction_count_max')->nullable();
            $table->bigInteger('estimated_transaction_amount_min')->nullable();
            $table->bigInteger('estimated_transaction_amount_max')->nullable();
            $table->integer('average_transaction_count_daily')->nullable();
            $table->integer('average_transaction_count_monthly')->nullable();
            $table->integer('average_transaction_count_yearly')->nullable();
            $table->bigInteger('average_transaction_amount_daily')->nullable();
            $table->bigInteger('average_transaction_amount_monthly')->nullable();
            $table->bigInteger('average_transaction_amount_yearly')->nullable();
            $table->string('cnic',13)->nullable();
            $table->boolean('basic')->nullable();
            $table->string('account_maintanece_certificate',255)->nullable();
            $table->string('delivery_mode_of_product_services',255)->nullable();
            $table->boolean('partnership')->nullable();
            $table->boolean('properitership')->nullable();
            $table->boolean('limited_company')->nullable();
            $table->string('ntn',255)->nullable();
            $table->string('partner_cnic',255)->nullable();
            $table->string('partnership_deed',255)->nullable();
            $table->string('certificate_of_incorporation',255)->nullable();
            $table->string('fbr_registration_notification',255)->nullable();
            $table->string('signed_letter_from_merchant_on_company_letter_head',255)->nullable();
            $table->string('board_resolution_to_verify_signatory_of_letter',255)->nullable();
        });







    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        $table->dropColumn(array('type_of_business','director_name','associated_group','group_name','group_introduction',
        'annual_report','nature_of_business','types_of_services_products','business_inception_date','delivery_mode_of_product_services',
        'goods_delivered_internationally','estimated_transaction_count_min','estimated_transaction_count_max','estimated_transaction_amount_min',
        'estimated_transaction_amount_max','average_transaction_count_daily','average_transaction_count_monthly','average_transaction_count_yearly',
        'average_transaction_amount_daily','average_transaction_amount_monthly','average_transaction_amount_yearly','cnic','basic','account_maintanece_certificate'
        ,'partnership','properitership','limited_company','ntn','partner_cnic','partnership_deed',
        'certificate_of_incorporation','fbr_registration_notification','signed_letter_from_merchant_on_company_letter_head',
        'board_resolution_to_verify_signatory_of_letter'));
        });
    }
}
