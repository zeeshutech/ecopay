<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('pp_Version',10)->nullable();
            $table->string('pp_Language',10)->nullable();
            $table->string('pp_MerchantID',30)->nullable();
            $table->string('pp_Password')->nullable();
            $table->decimal('pp_Amount',10,2);
            $table->string('pp_TxnCurrency',10)->nullable();
            $table->string('pp_TxnDateTime')->nullable();
            $table->string('pp_BillReference')->nullable();
            $table->string('pp_Description')->nullable();
            $table->string('pp_TxnExpiryDateTime')->nullable();
            $table->string('pp_ReturnURL')->nullable();
            $table->string('pp_SecureHash')->nullable();
            $table->string('pp_ResponseCode',20);
            $table->string('pp_ResponseMessage')->nullable();
            $table->string('pp_RetreivalReferenceNo')->nullable();
            $table->string('pp_BankID'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
