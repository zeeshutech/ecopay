<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('pp_TxnType',20)->after('pp_Language')->nullable();
            $table->string('pp_SubMerchantID',20)->after('pp_MerchantID')->nullable();
            $table->string('pp_TxnRefNo',30)->after('pp_SubMerchantID')->nullable();
            $table->string('pp_AuthCode',20)->after('pp_RetreivalReferenceNo')->nullable();
            $table->string('pp_SettlementExpiry',50)->after('pp_AuthCode')->nullable();
            $table->string('pp_ProductID',20)->after('pp_BankID')->nullable();
            $table->string('pp_CNIC',20)->after('pp_ProductID')->nullable();
            $table->string('pp_AccountNo',50)->after('pp_CNIC')->nullable();
            $table->string('ppmpf_1')->after('pp_AccountNo')->nullable();
            $table->string('ppmpf_2')->after('ppmpf_1')->nullable();
            $table->string('ppmpf_3')->after('ppmpf_2')->nullable();
            $table->string('ppmpf_4')->after('ppmpf_3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
        });
    }
}
