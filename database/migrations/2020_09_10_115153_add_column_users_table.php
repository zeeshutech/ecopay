<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('account_type',array('merchant','aggregator'))->after('remember_token')->nullable();
            $table->string('business_name',100)->nullable();
            $table->string('phone_number',11)->nullable();
            $table->string('business_location',50)->nullable();
            $table->string('website',255)->nullable();
            $table->string('physical_address',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(array('account_type','business_name','phone_number','business_location','website','physical_address'));
        });
    }
}
